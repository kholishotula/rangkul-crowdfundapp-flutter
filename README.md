# rangkul-crowdfundapp-flutter

Rangkul adalah aplikasi donasi di bidang pendidikan. Untuk saat ini, pengguna dapat melihat donasi dan detail donasi. Ke depannya, akan dikembangkan dengan menambah fitur pengguna yang dapat berdonasi, melihat donasi berdasarkan kategori, melihat donasi yang pernah dilakukan, dan membuat galang dana (donasi) baru.

Aplikasi ini menggunakan api yang saya buat sendiri, dan saya hosting di https://tranquil-headland-54481.herokuapp.com/

Dengan daftar api yang digunakan :
- /donasi untuk menampilkan data donasi keseluruhan
- /donasi-pilihan untuk menampilan data donasi pilihan. donasi dipilih berdasarkan tenggat waktu terdekat
- /donasi/:id untuk menampilkan detail donasi
- /donasi/:id/daftar-donatur untuk menampilkan daftar donatur dari sebuah donasi

Fitur :
- Login
- Register
- Melihat donasi pilihan
- Melihat daftar donasi
- Melihat detail donasi dan daftar donaturnya
- Melihat halaman profil
- Melihat halaman tentang developer
- Logout
