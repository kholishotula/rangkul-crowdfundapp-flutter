class PortfolioModel{
  final String foto;
  final String judul;

  PortfolioModel({this.foto, this.judul});
}

final List<PortfolioModel>items =[
  PortfolioModel(foto:'https://www.dropbox.com/s/55fau0o46fodps2/siternak.png?raw=1',judul:'SITERNAK'),
  PortfolioModel(foto:'https://www.dropbox.com/s/5lt1jd0x336y42h/techstore.png?raw=1',judul:'TechITStore'),
  PortfolioModel(foto:'https://www.dropbox.com/s/medlesgbjultfmh/Cliniheal.png?raw=1',judul:'Cliniheal'),
  PortfolioModel(foto:'https://www.dropbox.com/s/5y5z2raba5tv0x1/ismania%20bakery.jpg?raw=1',judul:'Ismania Bakery'),
  PortfolioModel(foto:'https://www.dropbox.com/s/puc9k7p8kgew67n/jahits.png?raw=1',judul:'Jahits'),
  PortfolioModel(foto:'https://www.dropbox.com/s/yn2c5lapoxhec4j/orecipes.png?raw=1',judul:'Orecipes'),
];