import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));
String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.nama,
    this.email,
    this.password,
    this.id,
    this.createdAt
  });

  String nama;
  String email;
  String password;
  String id;
  DateTime createdAt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    nama: json["nama"],
    email: json["email"],
    password: json["password"],
    id: json["id"],
    createdAt: DateTime.parse(json["createdAt"])
  );

  Map<String, dynamic> toJson() => {
    "nama": nama,
    "email": email,
    "password": password,
    "id": id,
    "createdAt": createdAt.toIso8601String()
  };
  
}