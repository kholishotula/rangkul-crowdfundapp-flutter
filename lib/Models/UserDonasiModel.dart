import 'dart:convert';

UserDonasiModel userDonasiModelFromJson(String str) => UserDonasiModel.fromJson(json.decode(str));
String userDonasiModelToJson(UserDonasiModel data) => json.encode(data.toJson());

class UserDonasiModel {
  UserDonasiModel({
    this.id,
    this.userId,
    this.donasiId,
    this.nominal,
    this.waktu_donasi,
    this.metode,
    this.createdAt
  });

  String id;
  String userId;
  String donasiId;
  String nominal;
  DateTime waktu_donasi;
  String metode;
  DateTime createdAt;

  factory UserDonasiModel.fromJson(Map<String, dynamic> json) => UserDonasiModel(
    id: json["id"],
    userId: json["userId"],
    donasiId: json["donasiId"],
    nominal: json["nominal"],
    waktu_donasi: DateTime.parse(json["waktu_donasi"]),
    metode: json["metode"],
    createdAt: DateTime.parse(json["createdAt"])
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "userId": userId,
    "donasiId": donasiId,
    "nominal": nominal,
    "waktu_donasi": waktu_donasi,
    "metode": metode,
    "createdAt": createdAt.toIso8601String()
  };
  
}