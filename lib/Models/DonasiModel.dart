import 'dart:convert';

import 'package:flutter/cupertino.dart';

DonasiModel donasiModelFromJson(String str) => DonasiModel.fromJson(json.decode(str));
String donasiModelToJson(DonasiModel data) => json.encode(data.toJson());

class DonasiModel {
  DonasiModel({
    this.id,
    this.judul,
    this.deskripsi,
    this.kategori,
    this.tingkatan,
    this.tgl_mulai,
    this.tgl_selesai,
    this.target,
    this.jumlah_kini,
    this.status,
    this.foto,
    this.nama_bank,
    this.no_rek,
    this.createdAt
  });

  String id;
  String judul;
  Text deskripsi;
  String kategori;
  String tingkatan;
  DateTime tgl_mulai;
  DateTime tgl_selesai;
  String target;
  String jumlah_kini;
  String status;
  String foto;
  String nama_bank;
  String no_rek;
  DateTime createdAt;

  factory DonasiModel.fromJson(Map<String, dynamic> json) => DonasiModel(
    id: json["id"],
    judul: json["judul"],
    deskripsi: json["deskripsi"],
    kategori: json["deskripsi"],
    tingkatan: json["tingkatan"],
    tgl_mulai: DateTime.parse(json["tgl_mulai"]),
    tgl_selesai: DateTime.parse(json["tgl_selesai"]),
    target: json["target"],
    jumlah_kini: json["jumlah_kini"],
    status: json["status"],
    foto: json["foto"],
    nama_bank: json["nama_bank"],
    no_rek: json["no_rek"],
    createdAt: DateTime.parse(json["createdAt"])
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "judul": judul,
    "deskripsi": deskripsi,
    "kategori": kategori,
    "tingkatan": tingkatan,
    "tgl_mulai": tgl_mulai.toIso8601String(),
    "tgl_selesai": tgl_selesai.toIso8601String(),
    "target": target,
    "jumlah_kini": jumlah_kini,
    "status": status,
    "foto": foto,
    "nama_bank": nama_bank,
    "no_rek": no_rek,
    "createdAt": createdAt.toIso8601String()
  };
  
}