import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingSatuScreen extends StatefulWidget {
  @override
  _LandingSatuScreenState createState() => _LandingSatuScreenState();
}

class _LandingSatuScreenState extends State<LandingSatuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 200),
                Image.asset(
                  'assets/img/logo.png',
                  width: 150,
                  height: 164,
                ),
                SizedBox(height: 10),
                Text(
                  "RANGKUL",
                  style: GoogleFonts.andada(
                    fontSize: 36,
                    color: Color(0xFFF369A8)
                  ),
                ),
                SizedBox(height: 50),
                Text(
                  "Bantu mereka agar bisa sekolah",
                  style: GoogleFonts.aBeeZee(
                    color: Color(0xFF3E3E3E),
                    fontSize: 14
                  ),
                ),
                Text(
                  "dan meraih cita-citanya",
                  style: GoogleFonts.aBeeZee(
                    color: Color(0xFF3E3E3E),
                    fontSize: 14
                  ),
                ),
                SizedBox(height: 200),
                Image.asset(
                  'assets/img/pageScroll1.png',
                  width: 80,
                  height: 16,
                ),
              ],
            ),
          ),
        ]
      ),
    );
  }
}