import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rangkulapp/Screen/MasterLayout.dart';
import 'package:rangkulapp/Screen/RegisterScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 168),
                  RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Image.asset("assets/img/minifiedLogo.png"),
                        ),
                        WidgetSpan(
                          child: Container(
                            padding: EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0),
                            child: Text(
                              "LOGIN",
                              style: GoogleFonts.aBeeZee(
                                fontSize: 36,
                                color: Color(0xFFF369A8)
                              ),
                            ),
                          )
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 56),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                    child: TextField(
                      controller: _emailController,
                      decoration: InputDecoration(  
                        prefixIcon: Icon(Icons.email),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF369A8))
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF9A3C9))
                        ),
                        labelText: "Email",
                        labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                    child: TextField(
                      obscureText: true,
                      controller: _passwordController,
                      decoration: InputDecoration(  
                        prefixIcon: Icon(Icons.lock),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF369A8))
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF9A3C9))
                        ),
                        labelText: "Password",
                        labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 25, right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: <Widget>[
                            Text(
                              'Belum punya akun?',
                              style: GoogleFonts.montserrat(
                                color: Color(0xFF3E3E3E)
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: GoogleFonts.montserrat(
                                  color: Colors.lightBlue[400]
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pushReplacement(MaterialPageRoute(
                                  builder: (context) => RegisterScreen()
                                ));
                              },
                              child: Text("Daftar"),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(  
                      style: ElevatedButton.styleFrom(
                        textStyle: GoogleFonts.montserrat(
                          color: Color(0xFF3E3E3E),
                          fontSize: 14
                        ),
                        primary: Color(0xFFF369A8),
                        minimumSize: Size(208, 41),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        )
                      ),
                      child: Text("Login"),
                      onPressed: () async {
                        await _firebaseAuth.signInWithEmailAndPassword(
                          email: _emailController.text,
                          password: _passwordController.text
                        ).then((value) =>
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => MasterLayout()
                          ))
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}