import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:rangkulapp/Screen/DonasiDetailScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final String urlGetAll = "https://tranquil-headland-54481.herokuapp.com/api/donasi";
  final String urlGetPilihan = "https://tranquil-headland-54481.herokuapp.com/api/donasi-pilihan";
  List allData;
  List pilihanData;

  @override
  void initState() {
    _getRefreshData();
    super.initState();
  }

  Future<void> _getRefreshData() async {
    this.getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var responseAll = await http.get(Uri.parse(urlGetAll), headers: {"Accept" : "application/json"});
    var responsePilihan = await http.get(Uri.parse(urlGetPilihan), headers: {"Accept" : "application/json"});
    // print(responseAll.body);
    if(mounted){
      setState(() {
        var convertDataToJson;
        if(responseAll.statusCode == 200){
          convertDataToJson = jsonDecode(responseAll.body);
          allData = convertDataToJson['data'];
        }
        if(responsePilihan.statusCode == 200){
          convertDataToJson = jsonDecode(responsePilihan.body);
          pilihanData = convertDataToJson['data'];
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: _getRefreshData,
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 8, bottom: 10),
                child: Row(
                  children: [
                    Text(
                      "Donasi Pilihan",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 215,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: pilihanData == null ? 0 : pilihanData.length,
                  itemBuilder: (context, index) {
                    if(pilihanData == null) {
                      return Container(
                        child: Text("Loading . . ."),
                      );
                    }
                    return Container(
                      width: 300,
                      child: Card(
                        elevation: 5,
                        margin: EdgeInsets.only(left:16,right:16),
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            children: [
                              pilihanData[index]['foto'] == null ? 
                                Image.asset("assets/img/bantuDia.png", height: 100)
                                : Image.network(pilihanData[index]['foto'], height: 100),
                              Padding(
                                padding: const EdgeInsets.all(8),
                                child: Text(
                                  pilihanData[index]['judul']
                                ),
                              ),
                              LinearPercentIndicator(
                                animation: true,
                                animationDuration: 1000,
                                lineHeight: 7,
                                percent: pilihanData[index]['jumlah_kini']/pilihanData[index]['target'],
                                linearStrokeCap: LinearStrokeCap.roundAll,
                                progressColor: Color(0xFFF9A3C9),
                                backgroundColor: Colors.grey[300],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Rp " + pilihanData[index]['jumlah_kini'].toString(),
                                    ),
                                    Text(
                                      pilihanData[index]['sisa_hari'].toString() + " hari lagi"
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.only(left: 8, bottom: 10),
                child: Row(
                  children: [
                    Text(
                      "Kategori",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8, right: 8),
                height: 100,
                child: GridView.count(
                  crossAxisCount: 4,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset("assets/img/kategori_biaya.png"),
                          onPressed: (){}
                        ),
                        Text(
                          "Biaya Pendidikan",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            color: Color(0xFF3E3E3E)
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset("assets/img/kategori_buku.png"),
                          onPressed: (){},
                        ),
                        Text(
                          "Buku",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            color: Color(0xFF3E3E3E)
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset("assets/img/kategori_seragam.png"),
                          onPressed: (){}
                        ),
                        Text(
                          "Seragam",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            color: Color(0xFF3E3E3E)
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset("assets/img/kategori_lainnya.png"),
                          onPressed: (){}
                        ),
                        Text(
                          "Lainnya",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            color: Color(0xFF3E3E3E)
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8, bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "#Rangkul",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFFF369A8)
                      ),
                    ),
                    Text(
                      "Mereka",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: ListView.builder(
                  physics: ClampingScrollPhysics(), 
                  shrinkWrap: true,
                  itemCount: allData == null ? 0 : allData.length,
                  itemBuilder: (BuildContext context, int index){
                    if(allData == null) {
                      return Container(
                        child: Text("Loading . . ."),
                      );
                    }
                    return Card(
                      elevation: 5,
                      margin: EdgeInsets.only(bottom: 8),
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: GestureDetector(
                          onTap: () {
                            Map<String, String> donasi = {
                              'id' : allData[index]['id'].toString(),
                              'judul' : allData[index]['judul'],
                              'deskripsi' : allData[index]['deskripsi'],
                              'sisa_hari': allData[index]['sisa_hari'].toString(),
                              'target': allData[index]['target'].toString(),
                              'jumlah_kini': allData[index]['jumlah_kini'].toString(),
                              'foto': allData[index]['foto'],
                              'nama_bank': allData[index]['nama_bank'],
                              'no_rek': allData[index]['no_rek']
                            };
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                  DonasiDetailScreen(donasi: donasi)
                              )
                            );
                          },
                          child: ListTile(
                            leading: allData[index]['foto'] == null ? 
                              Image.asset("assets/img/bantuDia.png")
                              : Image.network(allData[index]['foto']),
                            title: Padding(
                              padding: EdgeInsets.only(bottom: 8.0),
                              child: Text(
                                allData[index]['judul']
                              ),
                            ),
                            subtitle: Column(
                              children: [
                                LinearPercentIndicator(
                                  animation: true,
                                  animationDuration: 1000,
                                  lineHeight: 5,
                                  percent: allData[index]['jumlah_kini']/allData[index]['target'],
                                  linearStrokeCap: LinearStrokeCap.roundAll,
                                  progressColor: Color(0xFFF9A3C9),
                                  backgroundColor: Colors.grey[300],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Terkumpul"
                                      ),
                                      Text(
                                        "Sisa hari"
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Rp " + allData[index]['jumlah_kini'].toString()
                                      ),
                                      Text(
                                        allData[index]['sisa_hari'].toString()
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                )
              )
            ],
          ),
        ),
      ),
    );
  }
  
}