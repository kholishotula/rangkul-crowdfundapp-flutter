import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rangkulapp/Screen/LandingScreen.dart';
import 'package:rangkulapp/Screen/MasterLayout.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void initState() { 
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, (){
      if(FirebaseAuth.instance.currentUser != null){
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MasterLayout()
        ));
      }
      else{
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => LandingScreen()
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/img/logo.png',
              width: 188,
              height: 204,
            ),
            SizedBox(height: 20),
            Text(
              "RANGKUL",
              style: GoogleFonts.andada(
                fontSize: 36,
                color: Color(0xFFF369A8)
              )
            ),
            SizedBox(height: 20),
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}