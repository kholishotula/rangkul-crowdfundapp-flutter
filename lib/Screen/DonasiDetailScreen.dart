import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:percent_indicator/linear_percent_indicator.dart';

class DonasiDetailScreen extends StatefulWidget {
  Map<String, String> donasi;

  DonasiDetailScreen({ Key key, this.donasi }) : super(key: key);

  @override
  _DonasiDetailScreenState createState() => _DonasiDetailScreenState(donasi);
}

class _DonasiDetailScreenState extends State<DonasiDetailScreen> {
  Map<String, String> donasi;
  _DonasiDetailScreenState(this.donasi);

  List dataDonatur;
  
  urlGetDonatur(){
    return "https://tranquil-headland-54481.herokuapp.com/api/donasi/" + donasi['id'] + "/daftar-donatur";
  }

  @override
  void initState() {
    _getRefreshData();
    super.initState();
  }

  Future<void> _getRefreshData() async {
    this.getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var responseDonatur = await http.get(Uri.parse(urlGetDonatur()), headers: {"Accept" : "application/json"});
    // print(responseDonatur.body);
    setState(() {
      var convertDataToJson;
      if(responseDonatur.statusCode == 200){
        convertDataToJson = jsonDecode(responseDonatur.body);
        dataDonatur = convertDataToJson['data'];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFFF369A8),
        ),
        backgroundColor: Color(0xFFFFD7E8),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8),
              child: donasi['foto'] == null ? 
                      Image.asset("assets/img/bantuDia.png", height: 200)
                      : Image.network(donasi['foto'], height: 200),
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Text(
                donasi['judul'],
                style: GoogleFonts.aBeeZee(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF3E3E3E)
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(8, 15, 8, 15),
              child: Row(
                children: [
                  Text(
                    "Rp " + donasi['jumlah_kini'].toString(),
                    style: GoogleFonts.montserrat(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: Color(0xFFF369A8)
                    ),
                  ),
                  Text(
                    "  terkumpul dari Rp " + donasi['target'].toString(),
                    style: GoogleFonts.montserrat(
                      fontSize: 14,
                      color: Color(0xFF3E3E3E)
                    ),
                  )
                ],
              ),
            ),
            LinearPercentIndicator(
              animation: true,
              animationDuration: 1000,
              lineHeight: 7,
              percent: int.parse(donasi['jumlah_kini'])/int.parse(donasi['target']),
              linearStrokeCap: LinearStrokeCap.roundAll,
              progressColor: Color(0xFFF9A3C9),
              backgroundColor: Colors.grey[300],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: Text(
                donasi['sisa_hari'].toString() + " hari lagi",
                textAlign: TextAlign.end,
                style: GoogleFonts.montserrat(
                  color: Color(0xFF3E3E3E)
                ),
              ),
            ),
            SizedBox(height: 20),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12),
                    child: Text(
                      "Deskripsi Donasi",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 8, bottom: 15, top: 8),
                    child: Text(
                      donasi['deskripsi'],
                      style: GoogleFonts.montserrat(
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12),
                    child: Text(
                      "Daftar Donasi",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 18,
                        color: Color(0xFF3E3E3E)
                      ),
                    ),
                  ),
                  dataDonatur == null || dataDonatur.length == 0 ?
                    Container(
                      padding: EdgeInsets.only(left: 8, bottom: 15, top: 8),
                      child: Text(
                        "Belum ada donatur yang berdonasi",
                        style: GoogleFonts.montserrat(
                          color: Colors.red
                        ),
                      ),
                    )
                    : ListView.separated(
                      physics: ClampingScrollPhysics(), 
                      shrinkWrap: true,
                      itemCount: dataDonatur == null ? 0 : dataDonatur.length,
                      itemBuilder: (BuildContext context, int index){
                        return ListTile(
                          title: Text(dataDonatur[index]['nama'], style: TextStyle(fontWeight: FontWeight.w500),),
                          subtitle: Text("berdonasi sebesar Rp " + dataDonatur[index]['nominal'].toString()),
                          trailing: Text("pada " + dataDonatur[index]['waktu_donasi']),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index){
                        return Divider();
                      },
                    ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}