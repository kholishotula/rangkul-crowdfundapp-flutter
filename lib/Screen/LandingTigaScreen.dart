import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rangkulapp/Screen/LoginScreen.dart';
import 'package:rangkulapp/Screen/RegisterScreen.dart';

class LandingTigaScreen extends StatefulWidget {
  @override
  _LandingTigaScreenState createState() => _LandingTigaScreenState();
}

class _LandingTigaScreenState extends State<LandingTigaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 175),
                Text(
                  "Let's Get Started",
                  style: GoogleFonts.aBeeZee(
                    fontSize: 24,
                    color: Color(0xFFF369A8)
                  ),
                ),
                SizedBox(height: 20),
                Image.asset(
                  'assets/img/logo.png',
                  width: 114,
                  height: 124,
                ),
                SizedBox(height: 40),
                Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(  
                    style: ElevatedButton.styleFrom(
                      textStyle: GoogleFonts.montserrat(
                        color: Color(0xFF3E3E3E),
                        fontSize: 14
                      ),
                      primary: Color(0xFFF369A8),
                      minimumSize: Size(208, 41),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16))
                      )
                    ),
                    child: Text("Login"),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => LoginScreen()
                      ));
                    },
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(  
                    style: ElevatedButton.styleFrom(
                      textStyle: GoogleFonts.montserrat(
                        color: Colors.black,
                        fontSize: 14
                      ),
                      primary: Color(0xFFF9A3C9),
                      minimumSize: Size(208, 41),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                      )
                    ),
                    child: Text("Register"),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => RegisterScreen()
                      ));
                    },
                  ),
                ),
                SizedBox(height: 175),
                Image.asset(
                  'assets/img/pageScroll3.png',
                  width: 80,
                  height: 16,
                ),
              ],
            ),
          ),
        ]
      ),
    );
  }
}