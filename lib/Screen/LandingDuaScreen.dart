import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingDuaScreen extends StatefulWidget {
  @override
  _LandingDuaScreenState createState() => _LandingDuaScreenState();
}

class _LandingDuaScreenState extends State<LandingDuaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 225),
                Image.asset(
                  'assets/img/charity.png',
                  width: 150,
                  height: 164,
                ),
                SizedBox(height: 50),
                Text(
                  "Buat donasi, bagikan, dan capai target",
                  style: GoogleFonts.aBeeZee(
                    color: Color(0xFF3E3E3E),
                    fontSize: 14
                  ),
                ),
                Text(
                  "untuk membantu mereka",
                  style: GoogleFonts.aBeeZee(
                    color: Color(0xFF3E3E3E),
                    fontSize: 14
                  ),
                ),
                SizedBox(height: 225),
                Image.asset(
                  'assets/img/pageScroll2.png',
                  width: 80,
                  height: 16,
                ),
              ],
            ),
          ),
        ]
      ),
    );
  }
}