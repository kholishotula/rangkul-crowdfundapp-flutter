import 'package:flutter/material.dart';
import 'package:rangkulapp/Screen/LandingDuaScreen.dart';
import 'package:rangkulapp/Screen/LandingSatuScreen.dart';
import 'package:rangkulapp/Screen/LandingTigaScreen.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  PageController _controller = PageController(
    initialPage: 0,
  );

  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      children: [
        LandingSatuScreen(),
        LandingDuaScreen(),
        LandingTigaScreen()
      ],
    );
  }
}