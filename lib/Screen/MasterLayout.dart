import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:rangkulapp/Screen/HomeScreen.dart';
import 'package:rangkulapp/Screen/ProfileScreen.dart';

class MasterLayout extends StatefulWidget {
  @override
  _MasterLayoutState createState() => _MasterLayoutState();
}

class _MasterLayoutState extends State<MasterLayout> {
  int _selectedIndex = 0;
  final _layoutPage =[
    HomeScreen(),
    ProfileScreen()
  ];
  void _onTabItem(int index){
    setState((){
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset(
            "assets/img/minifiedLogo.png",
          ),
        ),
        backgroundColor: Color(0xFFFFD7E8),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Color(0xFFF369A8)
            ),
            onPressed: (){},
          )
        ],
      ),
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(  
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home"
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Akun"
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.add, color: Colors.white),
      //   backgroundColor: Color(0xFFF369A8),
      //   onPressed: (){},
      // ),
    );
  }
}