import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rangkulapp/Models/PortfolioModel.dart';

class AboutDeveloperScreen extends StatefulWidget {
  const AboutDeveloperScreen({ Key key }) : super(key: key);

  @override
  _AboutDeveloperScreenState createState() => _AboutDeveloperScreenState();
}

class _AboutDeveloperScreenState extends State<AboutDeveloperScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFFF369A8),
        ),
        backgroundColor: Color(0xFFFFD7E8),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8),
              child: Center(
                child: Text(
                  "Tentang",
                  style: GoogleFonts.aBeeZee(
                    fontSize: 30,
                    color: Color(0xFFF369A8)
                  ),
                )
              )
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        "assets/img/logo.png",
                        height: 50,
                      ),
                      Text(
                        "RANGKUL",
                        style: GoogleFonts.andada(
                          fontSize: 18,
                          color: Color(0xFFF369A8)
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      "DEVELOPER",
                      style: GoogleFonts.aBeeZee(
                        fontSize: 30,
                        color: Color(0xFFF369A8)
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Column(
                      children: [
                        Text(
                          "Kholishotul Amaliah",
                          style: GoogleFonts.montserrat(
                            fontSize: 20
                          ),
                        ),
                        Text(
                          "Software Developer",
                          style: GoogleFonts.montserrat(
                            fontSize: 18,
                            color: Color(0xFF3E3E3E)
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                        image: AssetImage("assets/img/maya.jpg")
                      )
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(12),
              child: Text(
                "Portfolio",
                style: GoogleFonts.aBeeZee(
                  fontSize: 18,
                  color: Color(0xFF3E3E3E)
                ),
              ),
            ),
            Container(
              height: 180,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Container(
                    width: 175,
                    child: Card(
                      elevation: 5,
                      margin: EdgeInsets.only(left:16,right:16),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Image.network(
                              items[index].foto,
                              height: 100
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: Text(
                                items[index].judul,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.montserrat(
                                  color: Colors.grey[600],
                                  fontSize: 15
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 12),
            Container(
              padding: EdgeInsets.all(12),
              child: Text(
                "Project Repository",
                style: GoogleFonts.aBeeZee(
                  fontSize: 18,
                  color: Color(0xFF3E3E3E)
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[
                        Image.network(
                          "https://cdn-icons-png.flaticon.com/512/25/25231.png",
                          height: 100,
                        ),
                        Text(
                          "@kholishotula",
                          style: GoogleFonts.montserrat(
                            color: Color(0xFF3E3E3E),
                            fontSize: 15
                          ),
                        )
                      ],
                    )
                  ),
                  SizedBox(width: 50),
                  Container(
                    child: Column(
                      children: <Widget>[
                        Image.network(
                          "http://assets.stickpng.com/images/5847f997cef1014c0b5e48c1.png",
                          height: 100,
                        ),
                        Text(
                          "@kholishotula",
                          style: GoogleFonts.montserrat(
                            color: Color(0xFF3E3E3E),
                            fontSize: 15
                          ),
                        )
                      ],
                    )
                  ),
                ]
              ),
            ),
            SizedBox(height: 12),
            Container(
              padding: EdgeInsets.all(12),
              child: Text(
                "Kontak",
                style: GoogleFonts.aBeeZee(
                  fontSize: 18,
                  color: Color(0xFF3E3E3E)
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Icon(Icons.mail_rounded),
                      SizedBox(width: 10),
                      Text(
                        "kholishotula.ka@gmail.com",
                        style: GoogleFonts.montserrat(
                          color: Color(0xFF3E3E3E),
                          fontSize: 14
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Icon(FontAwesomeIcons.instagram),
                      SizedBox(width: 10),
                      Text(
                        "@k_amaliah",
                        style: GoogleFonts.montserrat(
                          color: Color(0xFF3E3E3E),
                          fontSize: 14
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Icon(FontAwesomeIcons.telegram),
                      SizedBox(width: 10),
                      Text(
                        "@k_amaliah",
                        style: GoogleFonts.montserrat(
                          color: Color(0xFF3E3E3E),
                          fontSize: 14
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        )
      ),
    );
  }
}