import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:rangkulapp/Models/UserModel.dart';
import 'package:rangkulapp/Screen/LoginScreen.dart';
import 'package:rangkulapp/Screen/MasterLayout.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  // final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  UserModel _userModel;
  void _submit(BuildContext context) async {
    final String nama = _emailController.text.split("@")[0];
    final String email = _emailController.text;
    final String password = _passwordController.text;
    final UserModel userModel = await createUser(nama, email, password);

    if(mounted) {
      setState(() {
        _userModel = userModel;
      });
    }
  }

  Future<UserModel> createUser(String nama, String email, String password) async {
    try {
      var apiUrl = Uri.parse("https://tranquil-headland-54481.herokuapp.com/api/users");
      final response = await http.post(
        apiUrl,
        body: {
          "nama": nama,
          "email": email,
          "password": password
        }
      );
      if(response.statusCode == 201) {
        final String responseString = response.body;
        return userModelFromJson(responseString);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 125),
                  RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Image.asset("assets/img/minifiedLogo.png"),
                        ),
                        WidgetSpan(
                          child: Container(
                            padding: EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0),
                            child: Text(
                              "REGISTER",
                              style: GoogleFonts.aBeeZee(
                                fontSize: 36,
                                color: Color(0xFFF369A8)
                              ),
                            ),
                          )
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 80),
                  // Container(
                  //   padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                  //   child: TextField(
                  //     controller: _nameController,
                  //     decoration: InputDecoration(  
                  //       prefixIcon: Icon(Icons.person),
                  //       enabledBorder: OutlineInputBorder(
                  //         borderSide: BorderSide(color: Color(0xFFF369A8))
                  //       ),
                  //       focusedBorder: OutlineInputBorder(
                  //         borderSide: BorderSide(color: Color(0xFFF9A3C9))
                  //       ),
                  //       labelText: "Nama Lengkap",
                  //       labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                  //     ),
                  //   ),
                  // ),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                    child: TextField(
                      controller: _emailController,
                      decoration: InputDecoration(  
                        prefixIcon: Icon(Icons.email),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF369A8))
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF9A3C9))
                        ),
                        labelText: "Email",
                        labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                    child: TextField(
                      obscureText: true,
                      controller: _passwordController,
                      decoration: InputDecoration(  
                        prefixIcon: Icon(Icons.lock),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF369A8))
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFF9A3C9))
                        ),
                        labelText: "Password",
                        labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                      ),
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.fromLTRB(25, 8, 25, 8),
                  //   child: TextField(
                  //     obscureText: true,
                  //     controller: _passwordController,
                  //     decoration: InputDecoration(  
                  //       prefixIcon: Icon(Icons.lock),
                  //       enabledBorder: OutlineInputBorder(
                  //         borderSide: BorderSide(color: Color(0xFFF369A8))
                  //       ),
                  //       focusedBorder: OutlineInputBorder(
                  //         borderSide: BorderSide(color: Color(0xFFF9A3C9))
                  //       ),
                  //       labelText: "Konfirmasi Password",
                  //       labelStyle: TextStyle(color: Color(0xFF3E3E3E))
                  //     ),
                  //   ),
                  // ),
                  Container(
                    padding: EdgeInsets.only(left: 25, right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: <Widget>[
                            Text(
                              'Sudah punya akun?',
                              style: TextStyle(color: Color(0xFF3E3E3E)),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: TextStyle(color: Colors.lightBlue[400]),
                              ),
                              onPressed: () {
                                Navigator.of(context).pushReplacement(MaterialPageRoute(
                                  builder: (context) => LoginScreen()
                                ));
                              },
                              child: Text("Login"),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                  Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(  
                      style: ElevatedButton.styleFrom(
                        textStyle: GoogleFonts.montserrat(
                          color: Color(0xFF3E3E3E),
                          fontSize: 14
                        ),
                        primary: Color(0xFFF9A3C9),
                        minimumSize: Size(208, 41),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        )
                      ),
                      child: Text("Register"),
                      onPressed: () async {
                        _submit(context);
                        await _firebaseAuth.createUserWithEmailAndPassword(
                          email: _emailController.text,
                          password: _passwordController.text
                        ).then((value) =>
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => MasterLayout()
                          ))
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}