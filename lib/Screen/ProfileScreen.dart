import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rangkulapp/Screen/AboutDeveloperScreen.dart';
import 'package:rangkulapp/Screen/LandingTigaScreen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({ Key key }) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * .20,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/img/charity-hands.jpg"),
                          colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
                          fit: BoxFit.fitWidth
                        )
                      ),
                    ),
                  ],
                ),
                Container(
                  alignment: Alignment.topCenter,
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * .15,
                    left: 20
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 150,
                        height: 150,

                        // jika menggunakan foto profil

                        // decoration: BoxDecoration(
                        //   shape: BoxShape.circle,
                        //   image: new DecorationImage(
                        //     image: AssetImage("assets/img/maya.jpg")
                        //   )
                        // ),

                        // jika tanpa foto profil, tampilkan icon person
                        child: Image.asset(
                          "assets/img/account_icon.png"
                        )
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Column(
                          children: [
                            Text(
                              auth.currentUser.email.split("@")[0],
                              style: GoogleFonts.aBeeZee(
                                fontSize: 24,
                              ),
                            ),
                            Text(
                              auth.currentUser.email,
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                              )
                            ),
                            SizedBox(height: 10),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color(0xFFCCCCCC),
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                )
                              ),
                              onPressed: () {},
                              child: Text(
                                "Edit Profil"
                              )
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Donasi yang saya lakukan",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                          Text(
                            ">",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                        ]
                      ),
                    ),
                  ),
                ]
              )
            ),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Galang dana yang saya buat",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                          Text(
                            ">",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                        ]
                      ),
                    ),
                  ),
                ]
              )
            ),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                            AboutDeveloperScreen()
                        )
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Tentang Developer",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                          Text(
                            ">",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                        ]
                      ),
                    ),
                  ),
                ]
              )
            ),
            Card(
              color: Colors.white,
              elevation: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Bantuan",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                          Text(
                            ">",
                            style: GoogleFonts.aBeeZee(
                              fontSize: 18,
                              color: Color(0xFF3E3E3E)
                            ),
                          ),
                        ]
                      ),
                    ),
                  ),
                ]
              )
            ),
            SizedBox(height: 50),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  textStyle: GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 14
                  ),
                  primary: Color(0xFFF9A3C9),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  )
                ),
                child: Text(
                  "Logout",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  _signOut().then((value) => 
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => LandingTigaScreen()
                    ))
                  );
                },
              ),
            ),
          ],
        )
      ),
    );
  }
}